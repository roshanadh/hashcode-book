package np.com.roshanadhikary;

import java.io.*;
import java.util.*;

public class Submitter {
	public static void main(String[] args) throws IOException {
		BufferedWriter bw = new BufferedWriter(new FileWriter("SubmissionForA.txt"));
//		Stores dynamic list of books that are already scanned
		ArrayList<String> scannedBooks = new ArrayList<>();

//		Stores dynamic list of libraries that are already signed up
		ArrayList<Integer> signedUpLibraries = new ArrayList<>();

		Scanner scanner = new Scanner();
		int numOfLibraries = scanner.getNumOfLibraries();
		int numOfBooks = scanner.getNumOfBooks();
		int numOfScanningDays = scanner.getNumOfScanningDays();

		int[] daysForSignUp = scanner.getDaysForSignUp();
		int[] maxShipPerDay = scanner.getMaxShipPerDay();

		ArrayList<String> books = scanner.getBooks();

		int j = 0;
		for(int i = 0; i < numOfLibraries; i++) {
			if((j + daysForSignUp[i]) > numOfScanningDays) {
				System.out.println("Total libraries signed up " + i);
				System.out.println("Total scanning days used " + j);
//				Write the first line of submission file:
//				Number of Libraries to be signed up
//				bw.write("Number of libraries to be signed up");
//				bw.newLine();
				bw.write(i + "");
				bw.newLine();
				bw.flush();
				break;
			}
			else {
				j += daysForSignUp[i];
				signedUpLibraries.add(i);
			}
		}

		for(int i = 0; i < signedUpLibraries.size() - 1; i++) {
			System.out.println("Size: " + signedUpLibraries.size());

//			Write Library Index
//			bw.write("Library Index | Number of Books to be sent");
//			bw.newLine();
			bw.write(i + " ");

//			Write Number of Books to send for scanning
			int numOfBooksAvailable = books.get(i).split(" ").length;
			String[] booksAvailable = books.get(i).split(" ");

			int booksToSendForScan = 0;
			int[] booksToScan = new int[numOfBooksAvailable];
			for(int k = 0; k < numOfBooksAvailable; k++) {
				if(!scannedBooks.contains(booksAvailable[k])) {
					scannedBooks.add(booksAvailable[k]);
					booksToSendForScan++;
					booksToScan[k] = Integer.parseInt(booksAvailable[k]);
				}
			}

			bw.write( booksToSendForScan + "");
			bw.newLine();
//			bw.write("List of Books");
//			bw.newLine();
			for(int l = 0; l < booksToScan.length; l++) {
				bw.write(booksToScan[l] + " ");
			}
			bw.newLine();
			bw.flush();
		}
	}
}
