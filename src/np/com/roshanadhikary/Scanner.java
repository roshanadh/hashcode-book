package np.com.roshanadhikary;

import java.io.*;
import java.util.*;

public class Scanner {

	private int numOfBooks, numOfLibraries, numOfScanningDays;
	private String[] scores;
	private int[] daysForSignUp, maxShipPerDay;
	private ArrayList<String> libInfo, books;

	public Scanner() throws IOException {
		BufferedReader br = new BufferedReader(new FileReader("a_example.txt"));
		String line = br.readLine();

		String[] lineArr = line.split(" ");
		numOfBooks = Integer.parseInt(lineArr[0]);
		numOfLibraries = Integer.parseInt(lineArr[1]);
		numOfScanningDays = Integer.parseInt(lineArr[2]);
		System.out.println("The number of books is: " + numOfBooks);
		System.out.println("The number of libraries is: " + numOfLibraries);
		System.out.println("The number of scanning days is: " + numOfScanningDays);

		line = br.readLine();
		scores = line.split(" ");

//		libInfo contains: Number of books in each library, ...
//		... Days needed for each library to sign up, ...
//		... and the maximum number of books a library ...
//		... can ship in a day.
		libInfo = new ArrayList<>();

//		books contains: list of books inside each library
		books = new ArrayList<>();
		line = br.readLine();
		for(int lib = 0; lib < numOfLibraries; lib++){
			libInfo.add(line);
			line = br.readLine();
			books.add(line);
			line = br.readLine();
		}

		daysForSignUp = new int[libInfo.size()];
		maxShipPerDay = new int[libInfo.size()];
		for(int i = 0; i < libInfo.size(); i++) {
//			Days taken for each library to sign up:
//			daysForSignUp[0] is the days required for library 0 to sign up.
			daysForSignUp[i] = Integer.parseInt(libInfo.get(i).split(" ")[1]);
//			Maximum number of books that a library can ship in one day:
//			maxShipPerDay[0] is the max number of books that library 0 ...
//			... can ship in one day.
			maxShipPerDay[i] = Integer.parseInt(libInfo.get(i).split(" ")[2]);
		}
	}

	public static void main(String[] args) throws IOException {
		new Scanner();
	}

	public int getNumOfBooks() {
		return numOfBooks;
	}

	public int getNumOfLibraries() {
		return numOfLibraries;
	}

	public int getNumOfScanningDays() {
		return numOfScanningDays;
	}

	public String[] getScores() {
		return scores;
	}

	public int[] getDaysForSignUp() {
		return daysForSignUp;
	}

	public int[] getMaxShipPerDay() {
		return maxShipPerDay;
	}

	public ArrayList<String> getLibInfo() {
		return libInfo;
	}

	public ArrayList<String> getBooks() {
		return books;
	}
}
